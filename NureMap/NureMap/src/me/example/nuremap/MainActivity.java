package me.example.nuremap;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {

	Button search_rout,maps,info;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);
        
        search_rout = (Button) findViewById(R.id.searchRoutButton);
        maps = (Button) findViewById(R.id.mapButton);
        info = (Button) findViewById(R.id.infoButton);
    }
        
        
    public void onMapButtonClick(View view)  
    {
    	Intent a = new Intent(this, MapViewActivity.class);
        startActivity(a);
        return;
    }
    
    public void onRoutButtonClick(View view)  
    {
    	Intent a = new Intent(this, FromActivity.class);
        startActivity(a);
        return;
    }
    
    public void onInfoButtonClick(View view)  
    {
    	Intent a = new Intent(this, InfoActivity.class);
        startActivity(a);
        return;
    }
        
}
