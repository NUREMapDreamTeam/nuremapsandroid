package me.example.nuremap;

import java.util.HashMap;
import java.util.Map;

import android.os.Bundle;
import android.app.Activity;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

public class DiningRoomAcryvity extends Activity {
	private static final String TAG = "myLogs";
	String[] names = { "�������� 1 ����", "������� �������� 2 ����",
			"����� 2 ����", "�������� 3 ����", "����� 3 ����",
			"�������� 7 ������" };
	Map<String, String> list = new HashMap<String, String>();
	ListView lvMain;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dining_room);
		lvMain = (ListView) findViewById(R.id.lvMain);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, names);
		lvMain.setAdapter(adapter);
		
		lvMain.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// ���������� ������, ��� � ����, 
				//� ����� ����� ���������� ���� � ������, ���� 
				nameKey(names[position]);

				Toast toast = Toast.makeText(getApplicationContext(),
						nameKey(names[position]), Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// getMenuInflater().inflate(R.menu.dining_room_acryvity, menu);
		return true;
	}
	
	// ������� ��� void, � ��� ����� �������� �����
	private String nameKey(String s) {
		list.put("C������� 1 ����", "Key1");
		list.put("������� �������� 2 ����", "Key2");
		list.put("����� 2 ����", "Key3");
		list.put("�������� 3 ����", "Key4");
		list.put("����� 3 ����", "Key5");
		list.put("�������� 7 ������", "Key6");

		for (String entry : list.keySet()) {
			if (entry.toString() == s) {
				//entry.toString() - ��� "�������� �..."
				//list.get(entry.toString()) - ���� ��� ������
				return entry.toString() + "~" + list.get(entry.toString());
			}
		}
		return "";
	}

}
