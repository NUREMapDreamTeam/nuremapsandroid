package me.example.nuremap;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
public final class XmlCanteenReader {
	//API XmlCanteenReader.canteensRead(activity.getResourses.getXml(R.xml.canteens))
	/*��� �������� ������ �������� ������ �� ������ ���� Resourses � ������� getXml �� ���
	*/
	public static List<Canteen> canteensRead(XmlPullParser parser) 
			throws ParserConfigurationException, 
				XmlPullParserException, 
				IOException {
		List<Canteen> canteen = xmlReader(parser);
		return canteen;
		
	}
	private static Canteen readCanteen(XmlPullParser parser) throws XmlPullParserException, IOException{
		String name="", description="";
		int vertexID=0;
		HashMap<String, Double> menu = null;
		while(parser.next() != XmlPullParser.END_TAG){
			  if (parser.getEventType() != XmlPullParser.START_TAG) {
		            continue;
		        }
		        String tag = parser.getName();
		        if (tag.equals("VertexID")) {
		            vertexID = readVertexID(parser);
		        } else if (tag.equals("Name")) {
		            name = readName(parser);
		        } else if (tag.equals("Description")) {
		            description = readDescription(parser);
		        }else if(tag.equals("Menu")){
		        	menu = readMenu(parser);
		        } else {
		            skip(parser);
		        }
		}
		Canteen canteen = new Canteen(name, vertexID, description, null);
		Canteen.Menu canteenMenu = canteen.new Menu(menu);
		return null;
	}
	private static String readDescription(XmlPullParser parser) throws IOException, XmlPullParserException {
		return readTextTag(parser, "Description");
	}
	private static String readName(XmlPullParser parser) throws XmlPullParserException, IOException {
		return readTextTag(parser, "Name");
	}
	private static String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
	    String result = "";
	    if (parser.next() == XmlPullParser.TEXT) {
	        result = parser.getText();	        
	        parser.nextTag();
	    }
	    return result;
	}
	private static int readVertexID(XmlPullParser parser) throws XmlPullParserException, IOException {
		return Integer.parseInt(readTextTag(parser,"VertexID"));
	    
	}	
	private static String readTextTag(XmlPullParser parser, String target) throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, null, target);
	    String line = readText(parser);
	    parser.require(XmlPullParser.END_TAG, null, target);
	    return line;	
	}
	private static HashMap<String, Double> readMenu(XmlPullParser parser) throws IOException, XmlPullParserException {
		HashMap<String, Double> menu = new HashMap<String, Double>();		
		parser.require(XmlPullParser.START_TAG, null, "Menu");	    
	    while (parser.next() != XmlPullParser.END_TAG) {
	        if (parser.getEventType() != XmlPullParser.START_TAG) {
	            continue;
	        }
	        String name = parser.getName();
	        if (name.equals("Item")) {
	            menu.putAll(readMenuItem(parser, "Item"));	        
	        } else {
	            skip(parser);
	        }
	    }	    
		return menu;
		
	}
	private static HashMap<String, Double> readMenuItem(XmlPullParser parser, String target) throws IOException, XmlPullParserException{
		Double price=0.0;
		String product="";
		parser.require(XmlPullParser.START_TAG, null, target);
		if (parser.next() == XmlPullParser.TEXT) {
	    	price = Double.parseDouble(parser.getText());
	    	product = parser.getAttributeValue(0);
	        parser.nextTag();
	    }  		
	    parser.require(XmlPullParser.END_TAG, null, target);
	    HashMap<String, Double> items = new HashMap<String, Double>();
	    items.put(product, price);
	    return 	items;
	}
	private static List<Canteen> xmlReader(XmlPullParser parser) throws XmlPullParserException, IOException {
		List<Canteen> list = new ArrayList<Canteen>();		
		// ?null - XZ
		parser.require(XmlPullParser.START_TAG, null, "Canteens");	
		while(parser.next() != XmlPullParser.END_DOCUMENT){
			if (parser.getEventType() != XmlPullParser.START_TAG) {
	            continue;
	        }
			String name = parser.getName();
			if(name.equals("Canteen")){
				list.add(readCanteen(parser));
			}else{
				skip(parser);
			}
		}
		return list;
		
		
	}
	private static void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
	    if (parser.getEventType() != XmlPullParser.START_TAG) {
	        throw new IllegalStateException();
	    }
	    int depth = 1;
	    while (depth != 0) {
	        switch (parser.next()) {
	        case XmlPullParser.END_TAG:
	            depth--;
	            break;
	        case XmlPullParser.START_TAG:
	            depth++;
	            break;
	        }
	    }
	 }

}
