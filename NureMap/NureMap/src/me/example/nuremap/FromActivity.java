package me.example.nuremap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import me.example.nuremap.ClassRoom;
import me.example.nuremap.Graph;
import me.example.nuremap.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

public class FromActivity extends Activity {

	private static final int REQUEST_CODE = 10;
	Spinner fromCorpSpinner;
	RadioGroup from;
	Button fromAuditoryButton;

	RadioButton fromAud, fromEntry;

	char fromCorp;
	String fromPoint, toPoint;

	String aud;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.from_layout);

		fromCorpSpinner = (Spinner) findViewById(R.id.from_corp_Spinner);

		from = (RadioGroup) findViewById(R.id.from_radioGroup);
		fromAuditoryButton = (Button) findViewById(R.id.from_auditory_button);
		fromAud = (RadioButton) findViewById(R.id.from_auditory_radio);
		fromEntry = (RadioButton) findViewById(R.id.from_entry_radio);
		fromCorpSpinner.setAdapter(corpsComplete());
		/*
		 * spinnerLeft = (Spinner) findViewById(R.id.spinnerLeft); spinnerRight
		 * = (Spinner) findViewById(R.id.spinnerRight);
		 * spinnerLeft.setAdapter(CompleteAuditories());
		 * spinnerRight.setAdapter(CompleteAuditories()); clas = (RadioButton)
		 * findViewById(R.id.classroms); wc = (RadioButton)
		 * findViewById(R.id.wc); canteen = (RadioButton)
		 * findViewById(R.id.canteen); clas.setChecked(true);
		 */

		fromAuditoryButton.setText(getAuditories().get(0));
		aud = getAuditories().get(0);

		OnClickListener listener = new OnClickListener() {

			@Override
			public void onClick(View v) {

				switch (v.getId()) {
				case R.id.from_auditory_radio:
					fromAuditoryButton.setVisibility(View.VISIBLE);
					break;
				case R.id.from_entry_radio:
					fromAuditoryButton.setVisibility(View.INVISIBLE);
					break;
				default:
					break;
				}
			}
		};

		from.setOnClickListener(listener);
		fromAud.setOnClickListener(listener);
		fromEntry.setOnClickListener(listener);

	}

	private char whichCorp(String corp) {
		if (corp == "�������")
			return 'm';
		else if (corp == "�")
			return 'i';
		else if (corp == "�")
			return 'z';
		else
			return '0';
	}

	private ArrayAdapter<String> corpsComplete() {
		List<String> list = new ArrayList<String>();
		list.add("�������");
		list.add("�");
		list.add("�");
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.spinner_item, list);
		adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
		return adapter;
	}

	private ArrayList<String> getAuditories(/* String corps */) {
		ArrayList<String> array = new ArrayList<String>();
		Graph.init(getResources().openRawResource(R.raw.graph), getResources()
				.openRawResource(R.raw.classrooms), getResources()
				.openRawResource(R.raw.places));

		for (ClassRoom i : Graph.getLectureRooms().values()) {
			if (!(i.getName().contains("_")))
				array.add(i.getName());
		}
		Collections.sort(array);
		return array;
	}

	public void onNextButtonClick(View view) {
		SaveFromData();
		Intent i = new Intent(this, ToActivity.class);
		i.putExtra("fromCorp", fromCorp);
		i.putExtra("fromPoint", fromPoint);
		startActivity(i);
		return;
	}

	public void OpenAuditory(View view) {
		SaveFromData();
		Intent i = new Intent(this, AuditoryActivity.class);
		startActivityForResult(i, REQUEST_CODE);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK && requestCode == REQUEST_CODE) {
			if (data.hasExtra("auditory")) {
				fromAuditoryButton.setText(data.getExtras().getString(
						"auditory"));
				aud = data.getExtras().getString("auditory");
			} else
				fromAuditoryButton.setText(aud);
		}
	}

	private void SaveFromData() {
		fromCorp = whichCorp(fromCorpSpinner.getSelectedItem().toString());
		switch (from.getCheckedRadioButtonId()) {
		case R.id.from_auditory_radio:
			fromPoint = fromAuditoryButton.getText().toString();
			break;
		case R.id.from_entry_radio:
			fromPoint = "entry";
			break;
		default:
			break;
		}
	}
}

