package me.example.nuremap;

public class ClassRoom {
	private int VertexID;
	private String Name;
	private String Description;
	
    ClassRoom(String name, int id, String descr)
    {
    	VertexID = id;
        Name = name;
        Description = descr;
    }
    
	public int getVertexID() {
		return VertexID;
	}
	
	public String getName() {
		return Name;
	}
	
	public String getDescription() {
		return Description;
	}
}
