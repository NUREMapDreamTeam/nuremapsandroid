package me.example.nuremap;

public class User {
	 String name;
	 String address;
	 String location;

	 public String getName() {
	  return name;
	 }
	 public void setName(String name) {
	  this.name = name;
	 }
	 public String getValue() {
	  return location;
	 }
	 public void setValue(String location) {
	  this.location = location;
	 }
	 public User(String name, String location) {
	  super();
	  this.name = name;
	  this.location = location;
	 }

	}
