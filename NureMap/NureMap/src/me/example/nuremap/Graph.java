package me.example.nuremap;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class Graph
{
    public static Place[] getPlaces() {
        return places;
    }

    public static List<Vertex> getVertexList() {
        return vertexList;
    }

    public static Map<String, ClassRoom> getLectureRooms() {
        return lectureRooms;
    }

    private static Place[] places;
    private static List<Vertex> vertexList;
    private static Map<String, ClassRoom> lectureRooms;
    public static void init(InputStream gf, InputStream vf, InputStream pf)
    {
        vertexList = new ArrayList<Vertex>();
        vertexList = DataReader.LoadGraph(gf);
        lectureRooms = DataReader.ReadNames(vf);
        places = DataReader.addPlaces(pf);
    }
}
