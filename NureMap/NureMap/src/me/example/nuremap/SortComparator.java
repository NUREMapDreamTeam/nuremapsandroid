package me.example.nuremap;

import java.util.Comparator;

public class SortComparator implements Comparator<User> {
    public int compare(User p1, User p2) {
        return p1.getName().compareTo(p2.getName());
    }
}