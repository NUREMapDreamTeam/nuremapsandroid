package me.example.nuremap;

public class Place {
	private char placeName;
    public String placeID;

    public char getPlaceName() {
        return placeName;
    }

    public String getPlaceID() {
        return placeID;
    }

    public Place(char n, String p)
    {
        placeName = n;
        placeID = p;
    }
}
