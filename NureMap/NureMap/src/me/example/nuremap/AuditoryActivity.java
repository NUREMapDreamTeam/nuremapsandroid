package me.example.nuremap;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;

import java.util.ArrayList;
import java.util.Collections;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;

public class AuditoryActivity extends Activity {

	ListView userList;
	UserCustomAdapter userAdapter;
	ArrayList<User> userArray = new ArrayList<User>();
	ArrayList<User> searchList = new ArrayList<User>();
	EditText edittext;

	String auditory;
	String lastaud;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.auditory_layout);
		edittext = (EditText) findViewById(R.id.editText1);
		userList = (ListView) findViewById(R.id.listView);
		// ������������� �����.
		Graph.init(getResources().openRawResource(R.raw.graph), getResources()
				.openRawResource(R.raw.classrooms), getResources()
				.openRawResource(R.raw.places));

		for (ClassRoom i : Graph.getLectureRooms().values()) {
			if (!(i.getName().contains("_")))
				userArray.add(new User(i.getName(), i.getDescription()));
			Collections.sort(userArray, new SortComparator());
		}

		userAdapter = new UserCustomAdapter(AuditoryActivity.this,
				R.layout.actyvity_list_string, userArray);
		userList.setItemsCanFocus(false);

		userList.setAdapter(userAdapter);

		// ��� ���������� ����.
		userList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					final int position, long id) {

				if (edittext.getText().length() == 0)
					auditory = userArray.get(position).name;
				else
					auditory = searchList.get(position).name;
				finish();
			}
		});

		// ������ �������� �� ������, �����
		edittext.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				searchList.clear();
				for (ClassRoom i : Graph.getLectureRooms().values()) {
					if ((i.getName().startsWith(edittext.getText().toString()) || i
							.getDescription()
							.toLowerCase()
							.contains(
									edittext.getText().toString().toLowerCase()))
							&& !i.getName().contains("_"))
						searchList.add(new User(i.getName(), i.getDescription()));
				}
				Collections.sort(searchList, new SortComparator());
				userAdapter = new UserCustomAdapter(AuditoryActivity.this,
						R.layout.actyvity_list_string, searchList);
				userList.setAdapter(userAdapter);

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
			}
		});

	}

	public void onClickEditText(View v) {
		return;
	}

	@Override
	public void finish() {
		Intent data = new Intent();
		data.putExtra("auditory", auditory);
		setResult(RESULT_OK, data);
		super.finish();
	}
}
