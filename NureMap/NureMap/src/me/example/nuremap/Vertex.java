package me.example.nuremap;

import android.graphics.Point;

public class Vertex {
    public int getName() {
        return name;
    }

    public Vertex[] getNeighbors() {
        return neighbors;
    }

    public int getFloor() {
        return floor;
    }

    public Point getCoords() {
        return coords;
    }

    public void setNeighbors(Vertex[] neighbors) {
        this.neighbors = neighbors;
    }

	public String getCorp() {
		return corp;
	}

	public void setCorp(String corp) {
		this.corp = corp;
	}
	
    private int name;
	private Vertex[] neighbors;
	private int floor;
	private String corp;
    private Point coords;
    
	public Vertex(String ln)
	{
		name = Integer.parseInt(ln.split(" ")[0]);
	    String [] x = ln.split(" ");
        this.name = Integer.parseInt(x[0]);
        this.coords = new Point(Integer.parseInt(x[1]), Integer.parseInt(x[2]));
        this.floor = Integer.parseInt(x[4]);
        this.corp = x[3];
    }


}
