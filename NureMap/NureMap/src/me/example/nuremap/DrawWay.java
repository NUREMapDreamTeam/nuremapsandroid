package me.example.nuremap;

import java.util.List;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Paint.Style;

public class DrawWay {
	
	public static int GetFloor(int floor)
	{
		int floorId = 0;
		switch(floor) {
	    case 1:
	        floorId = R.drawable.main_first;
	        break;
	    case 2:
	        floorId = R.drawable.main_second;
	        break;
	    case 3:
	    	floorId = R.drawable.main_third;
	        break;
	    case 4:
	        floorId = R.drawable.main_fourth;
	        break;
		}
		return floorId;
	}
	
	public static Bitmap drawWayToBitmap(Context gContext, int resId, List<Point> list) {
		Resources resources = gContext.getResources();
		
		int iW = 1519;
        int iH = 1342;
        Bitmap bitmap;
        BitmapFactory.Options options = new BitmapFactory.Options();
        bitmap = BitmapFactory.decodeResource(resources, resId, options);
        bitmap = Bitmap.createScaledBitmap(bitmap, iW, iH, true); 
        Bitmap mutableBitmap = bitmap.copy(Bitmap.Config.ARGB_4444, true);
		Canvas canvas = new Canvas(mutableBitmap);
		Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		paint.setColor(Color.RED);
		paint.setStyle(Style.STROKE);
		paint.setStrokeWidth(5);
		Paint greenPaint = new Paint();
		Paint redPaint = new Paint();
		greenPaint.setColor(Color.GREEN);
		redPaint.setColor(Color.RED);
		for (int i = 0; i < list.size() - 1; i++) 
		{
			canvas.drawLine(list.get(i).x + 3, list.get(i).y, list.get(i+1).x + 3, list.get(i+1).y, paint);
			canvas.drawCircle(list.get(i).x + 3, list.get(i).y, 2, redPaint);
		}
		canvas.drawCircle(list.get(list.size()-1).x + 3, list.get(list.size()-1).y, 6, redPaint);
		canvas.drawCircle(list.get(0).x + 3, list.get(0).y, 6, greenPaint);	
		return mutableBitmap;
		}
}
