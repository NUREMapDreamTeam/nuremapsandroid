package me.example.nuremap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class GreetingActivity extends Activity  {
    protected  int time = 2000;

    @Override
    public  void  onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.greeting_layout);
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent i = new Intent(GreetingActivity.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        }, time);


    }


}
