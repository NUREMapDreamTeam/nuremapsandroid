package me.example.nuremap;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

public class ToActivity extends Activity {

	Spinner toCorpSpinner, toSpinner;
	RadioGroup to;
	Button toSelectButton;
	
	RadioButton auditoryRad, canteenRad, wcRad, exitRad;
	boolean fDiningRoom = false;
	char fromCorp, toCorp;
	String fromPoint, toPoint;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.to_layout);
		
		toCorpSpinner = (Spinner) findViewById(R.id.to_corp_spinner);
        to = (RadioGroup) findViewById(R.id.to_radioGroup);
        toSelectButton = (Button) findViewById(R.id.to_select_button);
        toSpinner = (Spinner)findViewById(R.id.to_spinner);
        auditoryRad = (RadioButton) findViewById(R.id.to_auditory_radio);
        canteenRad = (RadioButton) findViewById(R.id.to_canteen_radio);
        wcRad = (RadioButton) findViewById(R.id.to_wc_radio);
        exitRad = (RadioButton) findViewById(R.id.to_exit_radio);
        
        toCorpSpinner.setAdapter(corpsComplete());
        
    	OnClickListener listener = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				switch (v.getId()) {
				case R.id.to_auditory_radio:
					fDiningRoom = false;
					toSelectButton.setVisibility(View.VISIBLE);
					toSpinner.setVisibility(View.INVISIBLE);
					break;
				case R.id.to_exit_radio:
					toSelectButton.setVisibility(View.INVISIBLE);
					toSpinner.setVisibility(View.INVISIBLE);
					break;
				case R.id.to_wc_radio:
					toSelectButton.setVisibility(View.INVISIBLE);
					toSpinner.setVisibility(View.VISIBLE);
					toSpinner.setAdapter(wcComplete());
					break;
				case R.id.to_canteen_radio:
					fDiningRoom = true;
					toSelectButton.setText("�������� �������� !");
					toSelectButton.setVisibility(View.VISIBLE);
					toSpinner.setVisibility(View.INVISIBLE);
					break;
				default:
					break;
				}
			}
		};
		to.setOnClickListener(listener);
		auditoryRad.setOnClickListener(listener);
        canteenRad.setOnClickListener(listener);
        wcRad.setOnClickListener(listener);
        exitRad.setOnClickListener(listener);
	}
	
    private ArrayAdapter<String> corpsComplete()
    {
    	 List<String> list = new ArrayList<String>();
    	 list.add("�������");
    	 list.add("�");
    	 list.add("�");
       	 ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, list);
    	 adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
    	 return adapter;    
    }
    
    private ArrayAdapter<String> wcComplete()
    {
    	 List<String> list = new ArrayList<String>();
    	 list.add("�������");
    	 list.add("�������");
       	 ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, list);
    	 adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
    	 return adapter;    
    }
    
    public void onClickDining(View v) {
		if(fDiningRoom){
			Intent intentDining = new Intent(this,DiningRoomAcryvity.class);
			startActivity(intentDining);
		}
		return;
	}
}
