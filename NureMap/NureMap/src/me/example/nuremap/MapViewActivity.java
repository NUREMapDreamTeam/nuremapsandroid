package me.example.nuremap;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MapViewActivity extends Activity {
		
		FloorImageView img;
		int floor;
		TextView floorText;
		ArrayList<Integer> floors;
		
		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			floors = DisplayFloors('m');
			setContentView(R.layout.floor_view);
			int floor = 0;
			SetFloor(floor);
		}
		
		private void SetFloor(int floor)
		{
			DrawFloorNumber(floor+1);
	        img = (FloorImageView) findViewById(R.id.image);
	        img.setImageResource(floors.get(floor));
	        img.setMinZoom(1f);
	        img.setMaxZoom(5f);
	        //img.onRestoreInstanceState(img.onSaveInstanceState());
	        img.maintainZoomAfterSetImage(false);
	        //Ways();
		}
		
		private void DrawFloorNumber(int number)
		{
			switch(number){
			case 1:
				SplitText(R.string.one);
				break;
			case 2:	
				SplitText(R.string.two);
				break;
			case 3:
				SplitText(R.string.three);
				break;
			case 4:
				SplitText(R.string.four);
				break;
			case 5:
				SplitText(R.string.five);
				break;
			}
		}
		
		private void SplitText(int id)
		{
			String input = getString(id);
			String seperator = "\n";  
			String []tempText = input.split(seperator);
			floorText = (TextView) findViewById(R.id.textView1);
			if (input.contains(seperator)){
			        String b= "";
			        for (String c : tempText)
			            b +=c+"\n";
			        b= b.substring(0, b.length()-1);
			        floorText.setText(b);

			    }
			else{
			        floorText.setText(input);
			    }
		}

		public void onUpButtonClick(View view)  
		{ 
			if(floor < floors.size()-1)
				SetFloor(++floor);
			else
				return;
		} 
		
		public void onDownButtonClick(View view)  
		{  
			if(floor > 0)
				SetFloor(--floor);
			else
				return;
		} 
		
		public ArrayList<Integer> DisplayFloors(char corps) //m,z,i
		{
			ArrayList<Integer> floors = new ArrayList<Integer>();
			switch (corps) {
			case 'm':
				floors.add(R.drawable.main_first);
				floors.add(R.drawable.main_second);
				floors.add(R.drawable.main_third);
				floors.add(R.drawable.main_fourth);
				break;
			case 'z':
				break;
			case 'i':
				break;
			default:
				break;
			}
			return floors;
		}
}
