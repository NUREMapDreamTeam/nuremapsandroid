package me.example.nuremap;

import me.example.nuremap.PathCreator;
import me.example.nuremap.R;
import me.example.nuremap.FloorImageView;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.widget.Button;
import android.widget.TextView;

public class FloorViewActivity extends Activity {
	
	FloorImageView img;
	Path path;
	int floor;
	TextView floorText;
	char next;
	char last;
	Button btnUp;
	Button btnDown;
	boolean symbol;
	char endCh;
	Bitmap floorImage;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		String start = "";
		String end = "";
		Intent currentIntent;
		currentIntent = getIntent();
		Bundle extras = currentIntent.getExtras();
		if (extras!=null)
		{
			start = extras.getString("start");
				if(extras.getBoolean("symbol"))
				{
					symbol = true;
					endCh = extras.getChar("endCh");
				}
				else
					end = extras.getString("end");
		}
		else 
		{
			start = "null";
			end = "null";
		}
		Graph.init(getResources().openRawResource(R.raw.graph),
				getResources().openRawResource(R.raw.classrooms), 
				getResources().openRawResource(R.raw.places));
		if(symbol)
			path = PathCreator.GetPath(start, endCh);
		else
			path = PathCreator.GetPath(start, end);
		setContentView(R.layout.floor_view);
		int floor = 0;
		last ='1';
		//SetFloor(floor);
		
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
	    super.onConfigurationChanged(newConfig);
	}
	
	
	/*public void onUpButtonClick(View view)  
	{ 
		if(next =='u' && floor != path.getFloors().length - 1)
		{
			next = '1';
			last = 'u';
			floor++;
			SetFloor(floor);
		}
		else if(last == 'd' && floor > 0)
		{
			//last = 'u';
			floor--;
			SetFloor(floor);
		}
		else
			return;
	} 
	
	public void onDownButtonClick(View view)  
	{  
		if(next =='d' && floor != path.getFloors().length - 1)
		{
			next = '1';
			last = 'd';
			floor++;
			SetFloor(floor);
		}
		else if(last == 'u' && floor > 0)
		{
			//last = 'd';
			floor--;
			SetFloor(floor);
		}
		else
			return;
	} 
	
	private void SetFloor(int floor)
	{
		DrawFloorNumber(path.getFloors()[floor]);
        img = (FloorImageView) findViewById(R.id.image);
        floorImage = DrawWay.drawWayToBitmap(this, DrawWay.GetFloor(path.getFloors()[floor]), path.getLpoints()[floor]);
        img.setImageBitmap(floorImage);
        img.setMinZoom(1f);
        img.setMaxZoom(5f);
        //img.onRestoreInstanceState(img.onSaveInstanceState());
        img.maintainZoomAfterSetImage(false);
        Ways();
	}
	
	private void DrawFloorNumber(int number)
	{
		floorText = (TextView) findViewById(R.id.textView1);
		switch(number){
		case 1:
			floorText.setText(R.string.one);
			break;
		case 2:
			floorText.setText(R.string.two);
			break;
		case 3:
			floorText.setText(R.string.three);
			break;
		case 4:
			floorText.setText(R.string.four);
			break;
		case 5:
			floorText.setText(R.string.five);
			break;
		}
	}
	
	private void Ways()
	{
		btnUp = (Button) findViewById(R.id.upButton);
		btnDown = (Button) findViewById(R.id.downButton);
		if(floor < path.getFloors().length-1)
			if(path.getFloors()[floor] < path.getFloors()[floor+1])
			{
				next = 'u';
				btnUp.setBackgroundResource(R.drawable.floor_up_btn_a);
				btnDown.setBackgroundResource(R.drawable.floor_down_btn_na);
			}
			else
			{
				next = 'd';
				btnDown.setBackgroundResource(R.drawable.floor_down_btn_a);
				btnUp.setBackgroundResource(R.drawable.floor_up_btn_na);
			}
		else
		{
			btnUp.setBackgroundResource(R.drawable.floor_up_btn_na);
			btnDown.setBackgroundResource(R.drawable.floor_down_btn_na);
			next = '1';
			return;
		}
		
	}*/
		
}
		
	