package me.example.nuremap;

import java.util.HashMap;

import me.example.nuremap.ClassRoom;

import android.provider.MediaStore.Images;

public class Canteen extends ClassRoom{

	private Images photo;
	
	Canteen(String name, int id, String descr, Images photo) {
		super(name, id, descr);
		this.photo = photo;
	}
	public class Menu{
		HashMap <String, Double> menu;
		public Menu(HashMap<String, Double> menu){
			this.menu = menu;
		}
		public void add(String product, Double cost){
			menu.put(product, cost);
		}
	}

}
