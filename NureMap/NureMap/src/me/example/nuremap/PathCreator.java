package me.example.nuremap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PathCreator
{
	private static int pathLength(List<Vertex> list)
    {
		if (list == null) return 10000000;
        int r = 0;
        Vertex last = list.get(0);
        for (Vertex i : list)
        {
            ++r;
            r += (int)(Math.abs(i.getCoords().x - last.getCoords().x) + Math.abs(i.getCoords().y - last.getCoords().y));
            if (last.getFloor()!=i.getFloor())r+=24;
            last = i;
        }
        return r;
    }
    public static Path GetPath(String roomFrom, String roomTo)
    {
        int to, from;
        to = from = 0;
        if (Graph.getLectureRooms().containsKey(roomTo))
            to = Graph.getLectureRooms().get(roomTo).getVertexID();
        else
            throw new IllegalArgumentException("destination wrong");
        if (Graph.getLectureRooms().containsKey(roomFrom))
            from = Graph.getLectureRooms().get(roomFrom).getVertexID();
        else
            throw new IllegalArgumentException("source wrong");
        int[] p = Deijkstr(from);
        List<Vertex> res = new ArrayList<Vertex>();
        while (to > 0)
        {
            res.add(Graph.getVertexList().get(to));
            to = p[to];
        }
        Collections.reverse(res);
        return new Path(res);
    }
    public static Path GetPath(String roomFrom, char placeID)
    {
    	if (!Graph.getLectureRooms().containsKey(roomFrom))
            throw new IllegalArgumentException("source wrong");
        int from = Graph.getLectureRooms().get(roomFrom).getVertexID();
        List<Vertex> res = null;
        int[] p = Deijkstr(from);
        for (int i = 0; i < Graph.getPlaces().length; ++i)
        {
            List<Vertex> t = new ArrayList<Vertex>();
            if (Graph.getPlaces()[i].getPlaceName() != placeID) continue;
            int to = Graph.getLectureRooms().get(Graph.getPlaces()[i].getPlaceID()).getVertexID();
            while (to > 0)
            {
                t.add(Graph.getVertexList().get(to));
                to = p[to];
            }
            if (pathLength(t) < pathLength(res))
                res = t;
        }
        Collections.reverse(res);
        return new Path(res);
    }
    private static int[] Deijkstr(int start)
    {
        List<Vertex> v = Graph.getVertexList();
        int vn = v.size();
        boolean[] lb = new boolean[vn];
        int[] a = new int[vn];
        int[] from = new int[vn];
        for (int i = 0; i < a.length; ++i)
        {
            a[i] = 1000000;
            from[i] = -1;
            lb[i] = true;
        }
        a[start] = 0;
        while (true)
        {
            for (int i = 0; i < vn; ++i)
            {
                int l = (int)(Math.abs(v.get(i).getCoords().x - v.get(start).getCoords().x) +
                		Math.abs(v.get(i).getCoords().y - v.get(start).getCoords().y));
                if (v.get(i).getFloor()!=v.get(start).getFloor()||
                	v.get(i).getCorp()!=v.get(start).getCorp())
                    l=32;
                if (lb[i] && Contains(v.get(start).getNeighbors(),v.get(i))
                		&& a[i] > a[start] + l)
                {
                    a[i] = a[start] + l;
                    from[i] = start;
                }
            }
            lb[start] = false;
            start = -1;
            int min = 10000000, minn = -1;
            for (int i = 1; i < vn; ++i)
            {
                if (lb[i] && a[i] < min)
                {
                    min = a[i];
                    minn = i;
                }

            }
            start = minn;
            if (start == -1) break;
        }
        return from;
    }
	private static boolean Contains(Vertex[] neighbors, Vertex vertex) {
		for (Vertex i : neighbors)
			if (i.getName()== vertex.getName())
				return true;
		return false;
	}
}