package me.example.nuremap;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Point;

public class Path {
	class Location
    {
        public String Corp;
        public int Floor;
        public Location(String corp, int floor)
        {
            this.Corp = corp;
            this.Floor = floor;
        }
    }
	
    public List<Vertex> getV() {
        return v;
    }

    public int getNParts() {
        return nParts;
    }

    public Location[] getParts() {
        return parts;
    }

    public List<Point>[] getLpoints() {
        return lpoints;
    }

    private List<Vertex> v;
    private int nParts = 1;

    private Location[] parts;
    private List<Point> lpoints [];
    public Path(List<Vertex> v)
    {
    	
        this.v = v;
        Location lastl = new Location(v.get(0).getCorp(),v.get(0).getFloor());
        for (Vertex i : v)
            if (0!=lastl.Corp.compareTo(i.getCorp())||lastl.Floor != i.getFloor())
            {
                lastl = new Location(i.getCorp(), i.getFloor());
                ++nParts;
            }
        parts = new Location[nParts];
        lastl = new Location(v.get(0).getCorp(),v.get(0).getFloor());
        int j = 0;
        parts[j] = lastl;
        lpoints = new List[nParts];
        for (int i = 0; i < nParts; ++i)
            lpoints[i] = new ArrayList<Point>();
        for (Vertex i : v)
        {
        	if (0!=lastl.Corp.compareTo(i.getCorp())||lastl.Floor != i.getFloor())
            {
                ++j;
                lastl = new Location(i.getCorp(), i.getFloor());
                parts[j] = lastl;
            }
            lpoints[j].add(i.getCoords());
        }
    }
}
