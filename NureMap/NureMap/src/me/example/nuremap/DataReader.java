package me.example.nuremap;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DataReader  {
	private static Vertex Search( List<Vertex> v, int n)
    {
        for (Vertex i : v)
            if (i.getName() == n) return i;
        return null;
    }
    public static List<Vertex> LoadGraph(InputStream ins)
    {
    	BufferedReader br;
    	br = new BufferedReader(new InputStreamReader(ins));
    	List<Vertex> v = new  ArrayList<Vertex>();
        String s;
        List<String> brr = new ArrayList<String>();
        try{
        while ((s = br.readLine()) != null)
        {
            v.add(new Vertex(s));
            brr.add(s);
        }
        //br.reset();
        int k=0;
        int start = 5;
        while ((s = brr.get(k++)) != null)
            {
                String [] t = s.split(" ");
                Vertex el = Search(v, Integer.parseInt(t[0]));
                el.setNeighbors(new Vertex[t.length - start]);
                for (int i = start; i < t.length; ++i)
                {
                    el.getNeighbors()[i - start] = Search(v, Integer.parseInt(t[i]));
                }

            }}
        catch(Exception e){}
        return v;
    }
    public static Map <String, ClassRoom> ReadNames(InputStream ins)
    {
    	BufferedReader br;
    	br = new BufferedReader(new InputStreamReader(ins));
        Map <String, ClassRoom> auds = new HashMap<String, ClassRoom>();
        String s;
        try{
            while ((s = br.readLine()) != null)
            {
                String [] t = s.split(";");
                auds.put(t[0], new ClassRoom(t[0], Integer.parseInt(t[1]), t.length==2?" ":t[2]));
            }
        }
        catch(Exception e){}
        return auds;
    }
    public static Place[] addPlaces(InputStream ins)
    {
    	BufferedReader br;
    	br = new BufferedReader(new InputStreamReader(ins));
        String s,s1;
        try
        {
            s = br.readLine();
            int n = Integer.parseInt(s);
            Place[] places = new Place[n];
            for (int i = 0; i < n; ++i)
            {
                s = br.readLine();
                s1 = br.readLine();
                places[i] = new Place(s.charAt(0), s1);
            }
            return places;
        }
        catch (Exception e){return null;}
    }
}
